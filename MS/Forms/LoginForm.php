<?php
namespace MS\Forms;

use Kdyby\Translation\Translator;
use Nette\Security\User;

class LoginForm extends BaseForm {

	public function __construct($name = null,User $user, CustomeFormRenderer $customeFormRenderer,Translator $translator) {
		parent::__construct($name,null,$customeFormRenderer,$translator,$user);
		$this->addText('username');
		$this->addText('password');
	}

	public function submit($values) {
		$this->user->login($values->username,$values->password);
	}
}
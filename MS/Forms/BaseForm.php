<?php

namespace MS\Forms;

use Kdyby\Translation\Translator;
use Nette;
use Nette\Application\UI\Form;

abstract class BaseForm extends Form {

	protected $formName;
	protected $translator;
	protected $user;

	public function __construct(
		Nette\ComponentModel\IContainer $parent = null,
		$name = null,
		CustomeFormRenderer $customeFormRenderer,
		Translator $translator = null,
		Nette\Security\User $user) {
		parent::__construct($parent, $name);
		$this->setRenderer($customeFormRenderer);
		$this->translator = $translator;
		$this->user = $user;
	}

	public function defineForm() {

	}

	protected function t($msg) {
		return $this->translator->translate('messages.forms.' . $this->formName . '.' . $msg);
	}

	protected function save($values) {

	}



}
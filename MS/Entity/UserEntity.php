<?php


namespace MS\Entity;


class UserEntity {
	public $id;
	public $username;
	public $password;
	public $firstname;
	public $surname;
	public $class;
	public $isAbstract;
}
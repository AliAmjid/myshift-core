<?php


namespace MS\Entity;


class pages {

	public function getPages() {
		$pages = new \stdClass();
		//decleareing
		$dashboard = new page();
		$dashboard->url = "Dashboard:default";
		$dashboard->icon = "nc-icon nc-bank";
		$dashboard->resource = 'dashboard';
		$pages->dashboard = $dashboard;

		$user = new page();
		$user->url = 'User:default';
		$user->icon = "nc-icon nc-single-02";
		$user->resource = 'dashboard';
		$pages->user = $user;

		$group = new page();
		$group->url = 'Group:default';
		$group->icon = 'fas fa-users';
		$group->resource = 'group';
		$pages->group = $group;

		$subject = new page();
		$subject->url = 'Subject:default';
		$subject->resource = 'group';
		$subject->icon = 'fas fa-graduation-cap';
		$pages->subject = $subject;

		$setting = new page();
		$setting->url = 'Settings:default';
		$setting->resource = 'settings';
		$setting->icon = 'fas fa-cogs';
		$pages->settings = $setting;
		//returning pages
		return $pages;
	}

}
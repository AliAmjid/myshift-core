<?php


namespace MS\Mappers;


use Dibi\Connection;
use Nette\DI\Container;
use Tracy\Debugger;

abstract class Mapper extends Connection {

	protected $tableName;
	protected $entity;
	private $container;

	public function __construct($container = null) {
		$params = $container->getParameters()['dibi'];
		parent::__construct([
			'driver' => $params['driver'],
			'host' => $params['host'],
			'username' => $params['username'],
			'password' => $params['password'],
			'database' => $params['database'],
		]);
	}

	protected function setEntityName($name) {
		$this->entityName = $name;
	}

	public function loadEntityById(int $id) {
		return $this->mapOne($this->query('SELECT * FROM %n WHERE id = %i', $this->tableName, $id)->fetch());
	}

	protected function mapOne($row) {
		$entity = clone $this->entity;
		foreach (array_keys(get_object_vars($this->entity)) as $item) {
			if (!$row) {
				return null;
			}
			$entity->$item = $row->$item;
		}
		return $entity;
	}

	public function loadAll() {
		return $this->query('SELECT * FROM %n', $this->tableName)->fetchAll();
	}


	public function updateEntity($entity, $where = null) {
		if (!$where) {
			$this->query('UPDATE %n SET %a WHERE id = %i', $this->tableName, (array)$entity, $entity->id);
		} else {
			$this->query('UPDATE %n SET %a WHERE %and', $this->tableName, (array)$entity, $where);
		}

	}

	public function insetEntity($entity) {
		$this->query('INSERT INTO', $this->tableName, ' %v', (array)$entity);
	}

	public function loadById($id) {
		return $this->query('SELECT * FROM %n WHERE id = %i', $this->tableName, $id)->fetch();
	}


	public function lastItem() {
		return $this->mapOne($this->query('SELECT * FROM %n ORDER BY id DESC LIMIT 1', $this->tableName)->fetch());
	}

	public function destroy($entity) {
		$this->query('DELETE FROM %n WHERE id = %i', $this->tableName, $entity->id);
	}

	public function destroyByArgs(array $args) {
		return $this->query('DELETE FROM %n WHERE %and', $this->tableName, $args);

	}

	public function loadAllByArgs(array $args, $leftJoin = "") {
		return $this->query('SELECT * FROM %n %SQL WHERE %and', $this->tableName, $leftJoin, $args)->fetchAll();
	}

	public function loadByArgs(array $args, $leftJoin = "") {
		return $this->query('SELECT * FROM %n %SQL WHERE %and', $this->tableName, $leftJoin, $args)->fetch();

	}


}


<?php


namespace MS\Mappers;


use MS\Entity\aclResourceEntity;
use Nette\DI\Container;

class AclResourceMapper extends Mapper {
	protected $tableName = "acl_resource";
	protected $entity;
	public function __construct(Container $container, AclResourceEntity $entity) {
		parent::__construct($container);
		$this->entity = $entity;
	}

	public function loadByName() {
		return $this->query('SELECT * FROM %n ORDER by name ',$this->tableName)->fetchAll();
	}

}
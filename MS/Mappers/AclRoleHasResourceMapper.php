<?php


namespace MS\Mappers;


use MS\Entity\AclResourceEntity;
use Nette\DI\Container;

class AclRoleHasResourceMapper extends Mapper {
	protected $tableName = "acl_role_has_resource";
	protected $entity;
	public function __construct(Container $container, AclResourceEntity $aclResourceEntity) {
		parent::__construct($container);
		$this->entity = $aclResourceEntity;
	}
	public function loadByRoleId($id) {
		return $this->query('SELECT * FROM %n WHERE id_acl_role = %i',$this->tableName,$id);
	}

	public function checkIfResourceExist($idResource,$idRole) {
		if($this->query('SELECT * FROM %n WHERE id_acl_resource = %i AND id_acl_role = %i',$this->tableName,$idResource,$idRole)->count() == 1 ) {
			return true;
		} else {
			return false;
		}
	}
}
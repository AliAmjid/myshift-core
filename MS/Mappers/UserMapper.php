<?php


namespace MS\Mappers;


use MS\Entity\UserEntity;
use Nette\DI\Container;

class UserMapper extends Mapper {
	protected $tableName = "user";
	protected $entity;

	public function __construct(Container $container, UserEntity $userEntity) {
		parent::__construct($container);
		$this->entity = $userEntity;
	}

	public function loadbyUsername($username) {
		return $this->query('SELECT * FROM %n WHERE username = %s AND isAbstract = 0', $this->tableName, $username)->fetch();
	}

	public function updateRole($userId, $roleId) {
		$this->query('UPDATE user_has_role SET id_acl_role = %i WHERE id_user = %i', $roleId, $userId);
	}

	public function getUserRole($idUser) {
		return $this->query('SELECT id_acl_role FROM user_has_role WHERE id_user = %i', $idUser)->fetchSingle();
	}

	public function getUserRoleByName($id) {
		return $this->query('SELECT name FROM acl_role WHERE id = %i', $this->getUserRole($id))->fetchSingle();
	}

	public function searchForUsers($query) {
		$querys = explode(" ", $query);
		$finalyQuery = array();
		$finalyQuery[] = 'SELECT * FROM user WHERE';
		$i = 1;
		foreach ($querys as $query) {
			$qry = '
			lower(username) LIKE lower (%~like~) COLLATE [utf8_general_ci] 
			OR lower (surname) LIKE lower (%~like~) COLLATE [utf8_general_ci]
			OR lower (firstname) LIKE lower (%~like~) COLLATE [utf8_general_ci]
 			OR lower (class) LIKE lower (%~like~) COLLATE [utf8_general_ci]';
			if (empty($query) || $query == " ") {
				$qry = '0';
			}
			if ($i != count($querys)) {
				$qry .= ' OR';
			}
			$finalyQuery[] = $qry;
			$finalyQuery[] = $query;
			$finalyQuery[] = $query;
			$finalyQuery[] = $query;
			$finalyQuery[] = $query;
			$i++;
		}
		$finalyQuery[] = 'ORDER BY surname LIMIT 20';
		return $this->query(...$finalyQuery)->fetchAll();
	}

	public function countByArgs(array $args) {
		$this->query('SELECT ');
	}
}
<?php


namespace MS\Mappers;


use MS\Entity\UserHasRoleEntity;
use Nette\DI\Container;

class UserHasRoleMapper extends Mapper {
	protected $tableName = "user_has_role";
	protected $entity;

	public function __construct(Container $container, UserHasRoleEntity $userHasRoleEntity) {
		parent::__construct($container);
		$this->entity = $userHasRoleEntity;
	}

	public function loadRoleById($id) {
		return $this->query('SELECT name as role FROM user_has_role LEFT JOIN acl_role ON id_acl_role = acl_role.id WHERE id_user = %i',$id)->fetchAll();
	}

	public function countByIdUser($idUser) {
		return $this->query('SELECT * FROM %n WHERE id_user = %i',$this->tableName,$idUser)->count();
	}
}
<?php


namespace MS\Mappers;


use MS\Entity\AclRoleEntity;
use Nette\DI\Container;

class AclRoleMapper extends Mapper {
	protected $tableName = "acl_role";
	protected $entity;
	public function __construct(Container $container,AclRoleEntity $aclRoleEntity) {
		parent::__construct($container);
		$this->entity = $aclRoleEntity;
	}

	public function loadAll() {
		return $this->query('SELECT * FROM %n',$this->tableName)->fetchAll();
	}

}
<?php
namespace MS\Components;

use Kdyby\Translation\Translator;
use Nette\Application\UI\Control;
use Nette\Security\User;

class BaseControl extends Control {
	/** @persistent */
	public $local;
	public $translator;
	protected $user;

	public function __construct(User $user, Translator $translator) {
		$this->user = $user;
		$this->translator = $translator;
	}

}
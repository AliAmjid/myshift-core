<?php

namespace MS\Components\LoginComponent;

use Kdyby\Translation\Translator;
use MS\Components\BaseControl;
use MS\Entity\page;
use MS\Entity\pages;
use MS\Forms\LoginForm;
use Nette\Application\UI\Control;
use Nette\Security\AuthenticationException;
use Nette\Security\User;
use Tracy\Debugger;

class LoginComponent extends BaseControl {

	private $loginForm;

public function __construct(User $user, Translator $translator, LoginForm $loginForm) {
	parent::__construct($user, $translator);
	$this->loginForm = $loginForm;
}

	public function render() {
		$this->template->setFile(__DIR__ . "/default.latte");
		$this->template->render();

	}

	public function createComponentLoginForm() {
		$this->loginForm->onValidate[] = [$this,'loginFormSubmitted'];
		return $this->loginForm;
	}

	public function loginFormSubmitted($form,$values) {
		try {
			$this->loginForm->submit($values);
			$this->presenter->flashMessage('messages.login.success','success');
			$this->presenter->redirect('Dashboard:default');
		} catch (AuthenticationException $exception) {
			$this->flashMessage($this->translator->translate($exception->getMessage()),'error');
		}
	}
}
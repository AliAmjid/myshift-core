<?php

namespace MS\Components\MenuComponent;

use Kdyby\Translation\Translator;
use MS\Components\BaseControl;
use MS\Entity\page;
use MS\Entity\pages;
use Nette\Application\UI\Control;
use Nette\Security\User;
use Tracy\Debugger;

class MenuComponent extends BaseControl {
	private $pages;
	private $pagetitle;

	public function __construct(User $user, Translator $translator, pages $pages) {
		parent::__construct($user, $translator);
		$this->pages = $pages;
	}

	private function getPagesForRender() {
		$pagesForRender = array();
		/** @var page $page */
		foreach ($this->pages->getPages() as $page) {
			$page->name = $this->translator->translate('messages.pages.' . $page->url . ".name");
			if (true) {
				if ($this->presenter->getName() == explode(':', $page->url)[0]) {
					$page->isActive = true;
					$this->pagetitle = $this->translator->translate('messages.pages.' . $page->url . ".title");
				}
				$pagesForRender[] = $page;
				$this->user->isAllowed();

			}
		}
		return $pagesForRender;
	}


	public function render() {
		$this->template->pages = $this->getPagesForRender();
		$this->template->pagetitle = $this->pagetitle;
		$this->template->user = $this->user->getIdentity();
		$this->template->setFile(__DIR__ . "/default.latte");
		$this->template->render();

	}

	public function setPageTitle($name) {
		$this->pagetitle = $name;
	}
}
<?php

namespace MS;

use Kdyby\Translation\Translator;
use MS\Components\MenuComponent\MenuComponent;
use MS\Entity\page;
use MS\Entity\pages;
use Nette\Application\UI\Presenter;
use Nette\Security\User;

abstract class BasePresenter extends Presenter {
	/** @var User */
	protected $user;
	protected $hostName;
	protected $pagetitle;
	private $pages;
	private $menuComponent;
	/** @persistent */
	public $local;
	public $translator;

	public function __construct(
		User $user,
		pages $pages,
		MenuComponent $menuComponent,
		Translator $translator,
		\AuthorizatorFactory $authorizatorFactory
	) {
		$this->user = $user;
		$this->menuComponent = $menuComponent;
		$this->translator = $translator;
		$user->setAuthorizator($authorizatorFactory);
	}

	public function startup() {
		parent::startup();
		if (!$this->user->isLoggedIn()) {
			$this->flashMessage($this->t('login.autoLogOut'), 'info');
			$this->redirect('Homepage:default');
		}
		if (!$this->user->isAllowed(strtolower($this->getPresenter()->name))) {
			$this->flashMessage($this->t('basic.noPrivilage'), 'error');
			$this->redirect('Homepage:default');
		}
	}

	public function beforeRender() {
		$this->hostName = $this->getHttpRequest()->getUrl()->host;
		if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/version')) {
			$this->template->version = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/version');
		} elseif (file_exists($_SERVER['DOCUMENT_ROOT'] . 'version')) {
			$this->template->version = file_get_contents($_SERVER['DOCUMENT_ROOT'] . 'version');
		} else {
			$this->template->version = file_get_contents(__DIR__ . '/../../../../www/version');
		}
		$this->template->alerts = array();
	}

	public function createComponentMenu() {
		if (!empty($this->pagetitle) || isset($this->pagetitle)) {
			$this->menuComponent->setPageTitle($this->pagetitle);
		}
		return $this->menuComponent;
	}

	public function t($tr) {
		return $this->translator->translate('messages.' . $tr);
		$this->flashMessage();
	}

	protected function alert(
		$title,
		$html,
		$type = 'info',
		$showCloseButton = true,
		$showCancelButton = true,
		$confirmButtonText = "",
		$cancelButtonText = "",
		$confirmButtonAriaLabel = "",
		$cancelButtonAriaLabel = "",
		$focusConfirm = false
	) {
		$confirmButtonText = !empty($confirmButtonText) ? $confirmButtonText : $this->t('basic.defConfirmButton');
		$confirmButtonAriaLabel = !empty($confirmButtonAriaLabel) ? $confirmButtonAriaLabel : $this->t('basic.defConfirmLabel');
		$cancelButtonText = !empty($cancelButtonText) ? $cancelButtonText : $this->t('basic.defCancelButton');
		$cancelButtonAriaLabel = !empty($cancelButtonAriaLabel) ? $cancelButtonAriaLabel : $this->t('basic.defCancelLabel');

		$id = $this->getParameterId('alert');
		$messages = $this->getPresenter()->getFlashSession()->$id;
		$messages[] = $flash = (object)[
			'title' => $title,
			'html' => $html,
			'type' => $type,
			'showCloseButton' => $showCloseButton,
			'showCancleButton' => $showCancelButton,
			'focusConfirm' => false,
			'confirmButtonText' => $confirmButtonText,
			'cancelButtonText' => $cancelButtonText,
			'confirmButtonAriaLabel' => $confirmButtonAriaLabel,
			'cancelButtonAriaLabel' => $cancelButtonAriaLabel
		];
		$this->getTemplate()->alerts = $messages;
		$this->getPresenter()->getFlashSession()->$id = $messages;
		return $flash;
	}

	public function throwBackCasueOfPrivilae($where = 'Dashboard:default') {
		$this->flashMessage($this->t('basic.noPrivilage'), 'error');
		$this->redirect($where);
	}

	public function successMessage() {
		$this->flashMessage($this->t('basic.success'), 'success');
	}


}